import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_animarker/flutter_map_marker_animation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_basic/utils/image_to_bytes.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

List<LatLng> kLocations = [
  LatLng(
    -11.9760928,
    -77.06591809999999,
  ),
  LatLng(
    -11.9490478,
    -77.070402,
  ),
  LatLng(
    -11.9467494,
    -77.0704791,
  ),
  LatLng(
    -11.9457627,
    -77.070711,
  ),
  LatLng(
    -11.9503066,
    -77.08588349999999,
  ),
  LatLng(
    -11.9538071,
    -77.08542079999999,
  ),
  LatLng(
    -11.9543777,
    -77.0854198,
  ),
  LatLng(
    -11.9725239,
    -77.0814852,
  ),
];

class SimpleMarkerAnimationExample extends StatefulWidget {
  @override
  SimpleMarkerAnimationExampleState createState() =>
      SimpleMarkerAnimationExampleState();
}

class SimpleMarkerAnimationExampleState
    extends State<SimpleMarkerAnimationExample> {
  final initPosition = const CameraPosition(
      target: LatLng(
        -11.9768153,
        -77.0656747,
      ),
      zoom: 12);
  // final kMarkerId = MarkerId('MarkerId1');
  // final kDuration = Duration(seconds: 3);

  final markers = <MarkerId, Marker>{};

  final controller = Completer<GoogleMapController>();

  final stream = Stream.periodic(
    // kDuration,
    const Duration(seconds: 3),
    (count) {
      return [kLocations[count], kLocations[count + 1]];
    },
  ).take(kLocations.length);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Animarker(
      // curve: Curves.bounceOut,
      // rippleRadius: 0.2,
      shouldAnimateCamera: false,
      // useRotation: true,
      duration: const Duration(milliseconds: 2000),
      mapId: controller.future
          .then<int>((value) => value.mapId), //Grab Google Map Id
      markers: markers.values.toSet(),
      child: GoogleMap(
          mapType: MapType.normal,
          initialCameraPosition: initPosition,
          onMapCreated: (gController) {
            stream.forEach((value) =>
                newLocationUpdate(value.elementAt(0), value.elementAt(1)));

            controller.complete(gController);
            //Complete the future GoogleMapController
          }),
    );
  }

  void newLocationUpdate(LatLng nowlatLng, LatLng newLatLng) async {
    final iconoA =
        await imageToBytes('assets/ic_elrapido_76x153.png', size: 50);

    var marker = Marker(
        markerId: const MarkerId('MarkerId1'),
        position: nowlatLng,
        icon: BitmapDescriptor.fromBytes(iconoA),
        // ripple: true,
        // rotation: nowlatLng.latitude,
        // rotation: LatLng(nowlatLng.latitude, nowlatLng.longitude),
        rotation: Geolocator.bearingBetween(
          nowlatLng.latitude,
          nowlatLng.longitude,
          newLatLng.latitude,
          newLatLng.longitude,
        ),
        onTap: () {
          print('Tapped! $nowlatLng');
        });

    setState(() => markers[const MarkerId('MarkerId1')] = marker);
  }
}
