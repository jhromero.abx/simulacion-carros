import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/services.dart';

Future<Uint8List> imageToBytes(String path, {int size = 100}) async {
  final byteData = await rootBundle.load(path);
  final ImageConverted = byteData.buffer.asUint8List();

  final ImageResized =
      await ui.instantiateImageCodec(ImageConverted, targetWidth: size);

  final frame = await ImageResized.getNextFrame();
  final ImageByte = await frame.image.toByteData(
    format: ui.ImageByteFormat.png,
  );

  return ImageByte!.buffer.asUint8List();
}
