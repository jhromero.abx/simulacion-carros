import 'package:flutter/material.dart';
import 'package:google_maps_basic/map_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rutas Google',
      theme: ThemeData(primaryColor: Colors.blue),
      home: SimpleMarkerAnimationExample(),
    );
  }
}
